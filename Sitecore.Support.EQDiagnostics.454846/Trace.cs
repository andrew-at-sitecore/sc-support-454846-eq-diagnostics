﻿namespace Sitecore.Support
{
  using System;

  public class Trace
  {
    public static void Info(string message) {
      Sitecore.Diagnostics.Log.Info(Contextualize(message), new object());
    }

    private static string Contextualize(string message) {
      return $"[EQ#454846]: {message}";
    }
  }
}
