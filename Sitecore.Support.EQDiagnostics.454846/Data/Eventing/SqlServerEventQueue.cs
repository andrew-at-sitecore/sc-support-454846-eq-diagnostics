﻿namespace Sitecore.Support.Data.Eventing
{
  using System;
  using System.Globalization;
  using System.Linq;
  using Sitecore.Data;
  using Sitecore.Data.DataProviders.Sql;
  using Sitecore.Diagnostics;
  using Sitecore.Eventing;
  using Sitecore.StringExtensions;

  class SqlServerEventQueue : Sitecore.Data.Eventing.SqlServerEventQueue
  {
    public SqlServerEventQueue(SqlDataApi api) : base(api) { }

    public SqlServerEventQueue(SqlDataApi api, Database database) : base(api, database) { }

    public override void ProcessEvents(Action<object, Type> handler)
    {
      Assert.ArgumentNotNull(handler, "handler");
      if (this.ListenToRemoteEvents)
      {
        lock (this)
        {
          bool flag = false;
          QueuedEvent lastEvent = this.GetLastEvent();

          var queuedEventsList = this.GetQueuedEvents(this.InstanceName).ToList();

          var eqBatchSize = queuedEventsList.Count;
          if (eqBatchSize > 0)
          {
            string lastTimestamp = lastEvent?.Timestamp.ToString() ?? "NULL";
            string lastCreated = lastEvent?.Created.ToString(CultureInfo.InvariantCulture) ?? "NULL";
            Trace.Info("DETERMINED CURRENT LAST EVENT AS: [Timestamp: {0}, Created: {1}]".FormatWith(lastTimestamp, lastCreated));
            Trace.Info("START LOOP ON {0} ENTRIES".FormatWith(queuedEventsList.Count));
          }

          foreach (QueuedEvent event3 in queuedEventsList)
          {
            if ((event3.EventType == null) || (event3.InstanceType == null))
            {
              Log.Warn("Ignoring unknown event: " + event3.EventTypeName + ", instance: " + event3.InstanceTypeName + ", sender: " + event3.InstanceName, this);
            }
            else
            {
              handler(this.DeserializeEvent(event3), event3.InstanceType);
            }
            try
            {
              this.MarkProcessed(event3);
            }
            catch (Exception ex) {
              // silently catching exceptions to prevent "overloading" log files
            }
            flag = true;
          }

          if (eqBatchSize > 0)
          {
            Trace.Info("END LOOP");
          }

          if (!flag && (lastEvent != null))
          {
            try
            {
              this.SetTimestampForLastProcessing(new TimeStamp(lastEvent.Created, lastEvent.Timestamp));
            }
            catch (Exception ex) {
              // silently catching exceptions to prevent "overloading" log files
            }
          }
        }
      }
    }
    
    private object DeserializeEvent(QueuedEvent queuedEvent)
    {
      Assert.ArgumentNotNull(queuedEvent, "queuedEvent");
      return this.Serializer.Deserialize(queuedEvent.InstanceData, queuedEvent.InstanceType);
    }

  }
}
